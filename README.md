I am a woman who fell in love with a soldier. We both made our oaths to a life in the Army. Knowing the future would require much sacrifice and service to work. 
Never could I have imagined how much this would really be. 
Shortly after we joined, I was diagnosed with a terminal illness that changed our visions of the future drastically. 
